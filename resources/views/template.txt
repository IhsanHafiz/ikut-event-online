<div class="modal fade bd-example-modal-lg imagecrop" id="model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered lg d-flex flex-column align-items-center justify-content-center">
            <div class="modal-content modal-upload">
                <div class="modal-header d-flex flex-column">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row d-flex justify-content-center">
                    <h4>UPLOAD EVENT BANNER</h4>
                </div>
                <div class="modal-body">
                    <p>Adjust image ratio and swipe to change position</p>
                    <div class="row">
                        <div class="col-md-12 d-flex flex-column align-items-center justify-content-center">
                            <img id="image">
                        </div>
                    </div>
                    <div class="row row-footer d-flex">
                        <div class="col-6 d-flex justify-content-start">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        <div class="col-6 d-flex justify-content-end">   
                            <button type="button" class="btn btn-primary crop" id="crop">Crop</button>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>