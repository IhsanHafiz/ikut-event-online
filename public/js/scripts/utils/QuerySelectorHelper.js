const $ = {
  all(query) {
    return document.querySelectorAll(query);
  },

  first(query) {
    return document.querySelector(query);
  }
};

export default $;